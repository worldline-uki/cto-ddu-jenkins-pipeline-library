#!/usr/bin/groovy
package com.worldline

def getNameAndVersion(String buildFilePath) {

  String buildFileExtension = getFileExtension(buildFilePath)
  String buildFileContent = readFile buildFilePath
  String nameIdentifierRegex = getNameIdentifierRegex(buildFileExtension)
  String versionIdentifierRegex = getVersionIdentifierRegex(buildFileExtension)

  def (l1, nameContentPrefix, String buildName, nameContentSuffix) = (buildFileContent =~ /${nameIdentifierRegex}/)[0]
  def (l2, versionContentPrefix, String currentVersion, String featureSuffix, String snapshotSuffix, versionContentSuffix) = (buildFileContent =~ /${versionIdentifierRegex}/)[0]

  return [buildName, currentVersion, featureSuffix, snapshotSuffix]
}

static String getFileExtension(String buildFilePath) {
  return buildFilePath.substring(buildFilePath.lastIndexOf('.') + 1)
}

static String getVersionIdentifierRegex(String buildFileExtension) {
  // Identify the regex from the type of build/versioning file being processed
  // Regex has following four capturing groups (e.g. for an entry `   version = "1.0.1-SNAPSHOT"  `)
  // $1 -> version = "  (everything before version number)
  // $2 -> 1.0.1        (actual version number)
  // $3 -> +FEATURE     (feature suffix of version number if present)
  // $4 -> -SNAPSHOT    (snapshot suffix of version number if present)
  // $5 -> "            (everything after whole version number, i.e. quotes/whitespace)
  def versionIdentifierRegex
  switch (buildFileExtension) {
    case "sbt":
      // E.g. version := "1.1.3-SNAPSHOT"
      versionIdentifierRegex = "(\\s*version\\s*:=\\s*\")([\\d.]+)(\\+?[^\\-\\\"]*)(\\-?.*)(\"\\s*)"
      break
    case "json":
      // E.g. "version": "0.6.0"
      versionIdentifierRegex = "(\\s*\"version\"\\s*:\\s*\")([\\d.]+)(\\+?[^\\-\\\"]*)(\\-?.*)(\".*)"
      break
    case "xml":
      // E.g. <version>1.0.0-SNAPSHOT</version>
      versionIdentifierRegex = "(\\s*<version>\\s*)([\\d.]+)(\\+?[^\\-\\\\\"]*)(\\-?.*)(<\\/version>.*)"
      break
    default:
      versionIdentifierRegex = "(\\s*\"version\"\\s*:\\s*\")([\\d.]+)(\\+?[^\\-\\\"]*)(\\-?.*)(\".*)"
      break
  }
  return versionIdentifierRegex
}

static def getNameIdentifierRegex(String buildFileExtension) {
  // Identify the regex from the type of build/versioning file being processed
  // Regex has following four capturing groups (e.g. for an entry `   name = "my-service"  `)
  // $1 -> name = "     (everything before name)
  // $2 -> my-service   (actual name)
  // $3 -> "            (everything after name, i.e. quotes/commas/whitespace)
  def nameIdentifierRegex
  switch (buildFileExtension) {
    case "sbt":
      // E.g. name := "mmp-journeyplanning"
      nameIdentifierRegex = "(\\s*name\\s*:=\\s*\")(.*)(\"\\s*)"
      break
    case "json":
      // E.g. "name": "haproxy",
      nameIdentifierRegex = "(\\s*\"name\"\\s*:\\s*\")(.*)(\".*)"
      break
    case "xml":
      // E.g. <artifactId>path-traversal</artifactId>
      nameIdentifierRegex = "(\\s*<artifactId>\\s*)(.*)(<\\/artifactId>.*)"
      break
    default:
      nameIdentifierRegex = "(\\s*\"name\"\\s*:\\s*\")(.*)(\".*)"
      break
  }
  return nameIdentifierRegex
}

def bump(String buildFilePath, String bump, String versionSuffix) {
  // Extract the major/minor/patch number and bump
  def (buildName, version, featureSuffix, snapshotSuffix) = getNameAndVersion(buildFilePath)

  def major = version.substring(0, version.indexOf('.')).toInteger()
  def minor = version.substring(version.indexOf('.') + 1, version.lastIndexOf('.')).toInteger()
  def patch = version.substring(version.lastIndexOf('.') + 1).toInteger()

  switch (bump) {
    case "MAJOR":
      major = major + 1
      minor = 0
      patch = 0
      break
    case "MINOR":
      minor = minor + 1
      patch = 0
      break
    default:
      patch = patch + 1
      break
  }

  String newVersion = major + "." + minor + "." + patch + versionSuffix
  println "New version (${bump}) is $newVersion"

  updateVersion(buildFilePath, newVersion)

  return newVersion
}

def updateVersion(String buildFilePath, String newVersion) {

  String buildFileExtension = getFileExtension(buildFilePath)
  String versionIdentifierRegex = getVersionIdentifierRegex(buildFileExtension)

  String buildFileContent = readFile buildFilePath
  def newBuildFileContent = buildFileContent.replaceFirst(~/${versionIdentifierRegex}/, '$1' + newVersion + '$5')

  // Write the new version back to the version file
  writeFile([file: buildFilePath, text: newBuildFileContent])
}

return this