#!/usr/bin/groovy
package com.worldline

// Update an ECS Fargate service on a given cluster with a given image definition (version and config)
def deployTaskDef(String envPrefix, String envKey, Object taskDefSpec, String targetVersion) {

  def aws = new Aws()

  String imageName = taskDefSpec.name
  def containerDefSpecs = taskDefSpec.containers

  def defaultDependencyVersions = [:]
  def dependencyVersions = readProperties defaults: defaultDependencyVersions, file: 'dependencies'

  // Get the current task definition from AWS using task family - note - task def families are at account level - family
  // name therefore needs to be unique if same task running across envs/clusters
  String taskDefinitionFamily = "${envPrefix}-${envKey}-${imageName}"
  Object taskDefinition = aws.getTaskDefinition(taskDefinitionFamily)
  // Extract the container definitions from the AWS task definition
  def taskContainerDefs = taskDefinition.containerDefinitions

  Boolean configChange = false

  // Update the container definitions
  taskContainerDefs.each { taskContainerDef ->
    Boolean containerConfigChange = false
    // Find the container spec in the task spec container specs
    def containerSpec = containerDefSpecs.find { it.name == taskContainerDef.name }

    String currentImage = taskContainerDef.image
    String targetImageVersion = dependencyVersions."${containerSpec.name}" ?: targetVersion
    String targetImage = "${containerSpec.image}:${targetImageVersion}"
    if ( taskContainerDef.image != targetImage) {
      containerConfigChange = true
    }
    if (taskContainerDef.cpu != containerSpec.cpu || taskContainerDef.memory != containerSpec.memory) {
      containerConfigChange = true
    }
    if ( containerSpec.command != null && (taskContainerDef.command == null || taskContainerDef.command == containerSpec.command) ) {
      containerConfigChange = true
    }

    // Check for any change in environment variables
    def currentEnvVars = taskContainerDef.environment
    def newEnvVars = containerSpec.environment
    // Check if new env vars are same in task container def (i.e. no new/updated)
    newEnvVars.each { newEnvVar ->
      def existingEnvVar = currentEnvVars.find { it.name == newEnvVar.name }
      if ( existingEnvVar == null || existingEnvVar.value != newEnvVar.value ) {
        println("Env var ${newEnvVar.name} value ${newEnvVar.value} changed")
        containerConfigChange = true
      }
    }
    // Check if current task container def env vars are same in new (i.e. none deleted)
    currentEnvVars.each { currentEnvVar ->
      def newEnvVar = newEnvVars.find { it.name == currentEnvVar.name }
      if ( newEnvVar == null || newEnvVar.value != currentEnvVar.value ) {
        println("Existing env var ${currentEnvVar.name} value ${currentEnvVar.value} changed")
        containerConfigChange = true
      }
    }

    if (containerConfigChange) {
      println("Updating container definition to image ${targetImage} from ${currentImage}")
      taskContainerDef.image = targetImage
      taskContainerDef.cpu = containerSpec.cpu
      taskContainerDef.memory = containerSpec.memory
      if ( containerSpec.command != null ) {
        taskContainerDef.command = containerSpec.command
      }
      taskContainerDef.environment = newEnvVars
      configChange = true
    } else {
      println("No changes to container image ${currentImage} - skipping update of container")
    }
  }

  def newTaskDefinitionArn
  if (configChange) {
    println("Updating task definition.....")
    // Write the container definition back to AWS with updated task level settings - cpu + memory + containers
    String registeredTaskDefinitionJson = aws.updateTaskDefinition(taskDefinition, taskDefSpec.cpu, taskDefSpec.memory, taskContainerDefs)
    // Extract the new task definition ARN from the response
    def registeredTaskDefinition = readJSON text: registeredTaskDefinitionJson
    newTaskDefinitionArn = registeredTaskDefinition.taskDefinition.taskDefinitionArn
  } else {
    newTaskDefinitionArn = ""
  }
  return newTaskDefinitionArn
}


// Update an ECS Fargate service on a given cluster with a given task definition and version
def deployService(String envPrefix, String envKey, Object taskDefSpec, String targetVersion) {

  def aws = new Aws()

  def taskDefArn = deployTaskDef(envPrefix, envKey, taskDefSpec, targetVersion)
  if (taskDefArn != "") {
    println("Updating service ${taskDefSpec.name} .....")
    aws.updateServiceDefinition("${envPrefix}-${envKey}-ecs-cluster", taskDefSpec.name, taskDefSpec.desired_count, taskDefArn)
  } else {
    println("No updates to service ${taskDefSpec.name}.")
  }
}

// Update the target for a given event rule on a given cluster with a given image definition (version and config)
def deployEventTarget(String envPrefix, String envKey, Object taskDefSpec, String targetVersion) {

  def aws = new Aws()

  // Deploy the task definition for the event rule target
  def taskDefArn = deployTaskDef(envPrefix, envKey, taskDefSpec, targetVersion)
  // If the task def is updated (i.e. ARN provided), update the event rule target to the new task def
  if (taskDefArn != "") {
    String eventBusName = "${envPrefix}-${envKey}-event-bus"
    String ruleTargetId = "${envPrefix}-${envKey}-${taskDefSpec.name}-execution"

    def rules = aws.getRules(eventBusName)
    rules.Rules.each { rule ->
      Boolean ruleConfigChange = false
      def ruleTargets = aws.getRuleTargets(eventBusName, rule.Name)
      ruleTargets.Targets.each { target ->
        if (target.Id == ruleTargetId) {
          ruleConfigChange = true
          target.EcsParameters.TaskDefinitionArn = taskDefArn
          target.EcsParameters.TaskCount = taskDefSpec.desired_count.toInteger()
        }
      }
      if (ruleConfigChange) {
        println("Rule ${rule.Name} config change to target ${ruleTargetId} - updating targets for rule  .....")
        aws.updateRuleTargetDefinitions(eventBusName, rule.Name, ruleTargets)
      }
    }

  } else {
    println("No updates to targets for ${taskDefSpec.name}.")
  }
}

return this
