#!/usr/bin/groovy
package com.worldline

class TaskDefUpdate {
  def family
  def cpu
  def memory
  def executionRoleArn
  def taskRoleArn
  def containerDefinitions
  def volumes
}

// Retrieve the latest/active task definition for the given family
def getTaskDefinition(String taskFamily) {
  // Get the task definition - by family for the latest ACTIVE revision
  String taskDefinitionText = sh(returnStdout: true, script: "aws ecs describe-task-definition --task-definition ${taskFamily}").trim()
  def taskDefinitionJson = readJSON text: taskDefinitionText

  return taskDefinitionJson.taskDefinition
}

// Register a new task definition with updated container definitions for the given family
def updateTaskDefinition(Object taskDef, String cpu, String memory, Object containerDefs) {

  // Create json to pass to register-task-definition using --cli-input-json to set required non-static CLI params
  def taskDefUpdate = new TaskDefUpdate()
  taskDefUpdate.family = taskDef.family
  taskDefUpdate.cpu = cpu
  taskDefUpdate.memory = memory
  taskDefUpdate.executionRoleArn = taskDef.executionRoleArn
  taskDefUpdate.taskRoleArn = taskDef.taskRoleArn
  taskDefUpdate.containerDefinitions = containerDefs
  taskDefUpdate.volumes = taskDef.volumes

  // Write the task definition update json to file (to preserve loss of markup/quotes from string interpolation)
  writeJSON file: 'task-def.json', json: taskDefUpdate
  // Register the updated task definition in AWS - must set task memory to avoid container registration issue - https://github.com/aws/aws-cli/issues/4453
  String taskDefinitionJson = sh(returnStdout: true, script: "aws ecs register-task-definition --requires-compatibilities \"FARGATE\" --network-mode awsvpc --cli-input-json file://task-def.json").trim()
  // Remove the temporary json file
  sh "rm -f task-def.json"
  return taskDefinitionJson
}

// Update the active task definition for a given service in a given cluster
def updateServiceDefinition(String clusterName, String serviceName, String desiredCount, String taskDefinitionArn) {
  // Update the service task definition
  sh "aws ecs update-service --cluster ${clusterName} --service ${serviceName} --desired-count ${desiredCount} --task-definition ${taskDefinitionArn}"
}

// Retrieve the EventBridge rules on the given bus
def getRules(String eventBusName) {
  // Get the event rule targets
  String rulesText = sh(returnStdout: true, script: "aws events list-rules --event-bus-name ${eventBusName} --no-paginate").trim()
  def rulesJson = readJSON text: rulesText
  return rulesJson
}

// Retrieve the EventBridge targets for the given rule on the given bus
def getRuleTargets(String eventBusName, String ruleName) {
  // Get the event rule targets
  String ruleTargetsText = sh(returnStdout: true, script: "aws events list-targets-by-rule --event-bus-name ${eventBusName} --rule ${ruleName} --no-paginate").trim()
  def ruleTargetsJson = readJSON text: ruleTargetsText
  return ruleTargetsJson
}

// Update the target task definition for a given event bridge rule
def updateRuleTargetDefinitions(String eventBusName, String ruleName, Object eventTargetsJson) {

  // Write the event rule targets json to file (to preserve loss of markup/quotes from string interpolation)
  writeJSON file: 'targets.json', json: eventTargetsJson
  // Register the updated event rule target in AWS
  String eventTargetsDefinitionJson = sh(returnStdout: true, script: "aws events put-targets --event-bus-name ${eventBusName} --rule ${ruleName} --cli-input-json file://targets.json").trim()
  // Remove the temporary json file
  sh "rm -f targets.json"
  return eventTargetsDefinitionJson
}

return this
