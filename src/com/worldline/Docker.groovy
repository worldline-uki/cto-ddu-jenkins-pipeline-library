#!/usr/bin/groovy
package com.worldline

def dockerBuild(String imageName, String dockerFilePath = ".", Boolean useCache = true) {
  if (useCache) {
    sh "docker build -t ${imageName} ${dockerFilePath}"
  } else {
    sh "docker build --pull --no-cache -t ${imageName} ${dockerFilePath}"
  }
}

def dockerTagAndPush(String imageName, String tag) {
  sh "docker tag ${imageName} ${tag}"
  sh "docker push ${tag}"
}

def dockerPrune(String imageName) {
  sh "docker rmi --force \$(docker images -q ${imageName} | uniq)"
}

def dockerLoginEcr(String awsRegion, String awsEcr) {
  sh "aws ecr get-login-password --region ${awsRegion} | docker login --username AWS --password-stdin ${awsEcr}"
}

def createDockerContext(String context, String dockerHost) {
  sh "docker context create ${context} --docker host=tcp://${dockerHost}:2375"
}

def deleteDockerContext(String context) {
  sh "docker context rm ${context}"
}

//  Assumes environment vars NEXUS_USER and NEXUS_PASSWORD
def dockerLoginNexus(String context, String nexusUrl, String nexusUsername, String nexusPassword) {
  sh """
    export DOCKER_CONTEXT=${context}
    docker login -u ${nexusUsername} -p ${nexusPassword} ${nexusUrl}
  """
}

// Deploy a docker-stack.yml to a swarm, must have named context pre-created
def deploySwarm(String context, String dockerStackId, String dockerStackYmlFile) {
  // Deploy the stack definition using the given context
  sh """
    export DOCKER_CONTEXT=${context}
    docker stack deploy --compose-file ${dockerStackYmlFile} --with-registry-auth ${dockerStackId}
  """
}

def mergeStackDefinitions(String baseStackDef = "swarm/docker-stack-base.yml", String branchStackDef = "swarm/docker-stack.yml") {
  def newStackDef = "versioned-docker-stack.yml"
  sh """
    export \$(grep -v '^#' versions)
    docker-compose -f ${baseStackDef} -f ${branchStackDef} config > ${newStackDef}
  """
  return newStackDef
}

return this
