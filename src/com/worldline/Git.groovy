#!/usr/bin/groovy
package com.worldline

def getBitbucketUrl() {
  // Get the url for the current repo
  // Note - Bitbucket repo contains the username:password@ (i.e. x-auth-token generated from OAuth credentials)
  sh returnStdout: true, script: "echo `git config remote.origin.url`  | sed 's~http[s]*://~~g'  | tr -d '\n'"
}

def tagVersion(String version) {
  // Tag the version in GitHub
  def gitRepo = getBitbucketUrl()
  sh "git tag -a ${version} -m \"Jenkins - Tagged version ${version}\""
  sh "git push https://${gitRepo} --tag '${version}'"
  println "Tagged release version " + version + " in git repository"
}


def gitCommit(String filePath, String commitMessage) {
  // Commit the updated version file to Git
  def gitRepo = getBitbucketUrl()
  sh "git add ${filePath}"
  sh "git commit -m \"${commitMessage}\""
}

def gitPush() {
  // Push any commits to the Bitbucken repo
  def gitRepo = getBitbucketUrl()
  sh "git push https://${gitRepo}"
}

return this