#!/usr/bin/env groovy
import com.worldline.Docker

/**
 * Build Docker image <imageName> based on Dockerfile in current directory
 */
def call(String imageName, String nexusUrl = "", String nexusUsername = "", String nexusPassword = "", Boolean useCache = true) {

  def docker = new Docker()

  // Login to Nexus to allow pull of private images if url etc provided
  if (!nexusUsername?.isEmpty() && !nexusUsername?.isEmpty() && !nexusPassword?.isEmpty()) {
    docker.dockerLoginNexus("default", nexusUrl, nexusUsername, nexusPassword)
  }
  // The default (i.e. local swarm engine) context can be used for the build
  docker.dockerBuild(imageName, ".", useCache)
}
