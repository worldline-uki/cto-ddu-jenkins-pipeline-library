#!/usr/bin/env groovy
import com.worldline.BuildFile
import com.worldline.Docker

/**
 * Publish an image to AWS ECR - image name/tag created from BuildFile metadata
 * builtImageName - Name of already built image to be published
 * buildFilePath - Path of build file to identify name and version
 * nexusUrl - URL of nexus instance to publish to
 * nexusUsername, nexusPassword - Nexus credentials (if required)
 */
def call(String builtImageName, String buildFilePath, String nexusUrl,  String nexusUsername = "", String nexusPassword = "") {

  def docker = new Docker()
  def buildFile = new BuildFile()

  // Retrieve name/version information from build file
  def (String name, String baseVersion, String feature, String snapshot) = buildFile.getNameAndVersion(buildFilePath)

  // Set target image name for publishing incl full repo name
  // Note - A tag name in Nexus must be valid ASCII and may contain lowercase and uppercase letters, digits,
  // underscores, periods and dashes -> Default semantic versioning uses "+" for feature so need to replace
  String tag = "${nexusUrl}/${name}:${baseVersion}${feature}${snapshot}".replaceAll('\\+','-')
  println "Tagging docker image as ${tag}"

  // The default (i.e. local swarm engine) context can be used for the push
  if (!nexusUsername?.isEmpty() && !nexusPassword?.isEmpty()) {
    docker.dockerLoginNexus("default", nexusUrl, nexusUsername, nexusPassword)
  }
  docker.dockerTagAndPush(builtImageName, tag)
}
