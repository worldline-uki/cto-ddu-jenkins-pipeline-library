#!/usr/bin/env groovy
import com.worldline.BuildFile

/**
 * Update build file with feature extension to buildfile
 */
def call (String buildFilePath, String feature) {

  def buildFile = new BuildFile()

  def (name, String version, oldFeature, String snapshot) = buildFile.getNameAndVersion(buildFilePath)

  String featureVersion = version + "+" + feature + snapshot
  println "Feature version (${feature}) is $featureVersion"

  // Update to the bumped snapshot version in the build file
  buildFile.updateVersion(buildFilePath, featureVersion)
}
