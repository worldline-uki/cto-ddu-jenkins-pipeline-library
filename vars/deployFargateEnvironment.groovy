#!/usr/bin/env groovy
import com.worldline.Environment

/**
 * Deploy versioned services to AWS ECS Fargate for specified environment
 */
def call(String envPrefix, String envKey) {

  def environment = new Environment()

  // Read versions properties file and update services
  def serviceVersionsExists = fileExists 'versions'
  if (serviceVersionsExists) {
    def versions = readProperties file: 'versions'
    versions.each { appVersion ->
      println("Deploying Fargate service ${appVersion.key}...")

      // Read the task def spec target values from the repo service-defs
      def taskSpecFilePath = "service-defs/${appVersion.key}.json"
      def taskDefSpec = readJSON file: taskSpecFilePath

      environment.deployService(envPrefix, envKey, taskDefSpec, appVersion.value)
    }
  } else {
    println("No versions file exists - skipping Fargate services deployment....")
  }

  def eventVersionsExists = fileExists 'events'
  if (eventVersionsExists) {
    def eventDefs = readProperties file: 'events'
    eventDefs.each { appVersion ->
      println("Deploying EventBridge target ${appVersion.key}...")

      // Read the task def spec target values from the repo service-defs
      def taskSpecFilePath = "task-specs/${appVersion.key}.json"
      def taskDefSpec = readJSON file: taskSpecFilePath

      environment.deployEventTarget(envPrefix, envKey, taskDefSpec, appVersion.value)
    }
  } else {
    println("No events file exists - skipping Event bridge target deployment....")
  }

}
