#!/usr/bin/env groovy

/**
 * Complete pipeline to build/package/tag/release a Maven/Java module
 * Pipeline assumes existence of build.sbt in repo root directory
 * Assumes Nexus_Credentials set in Jenkins
 */
def call () {
    pipeline {
        agent {
            ecs {
                inheritFrom 'fargate-jenkins-agent-java'
            }
        }
        parameters {
            choice(choices: 'Default\nRelease', description: 'Choose action?', name: 'ACTION')
            choice(choices: 'NA\nPATCH\nMINOR\nMAJOR', description: 'Bump for next snapshot version', name: 'BUMP')
            choice(choices: 'DefinitelyNot\nForcePublish', description: 'Force publish ignoring unit test results', name: 'FORCE_PUBLISH')
        }
        stages {
            stage('Checkout') {
                steps {
                    deleteDir()
                    checkout scm
                }
            }
            stage('Build & Unit Test') {
                steps {
                    sh 'mvn clean compile test'
                }
            }
            stage('Set release version & tag repo') {
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    tagRelease("pom.xml")
                }
            }
            stage('Assemble') {
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature*'
                    }
                }
                steps {
                    sh 'mvn package -DskipTests=true'
                }
            }
            stage('Publish module') {
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature*'
                    }
                }
                steps {
                    sh "mvn deploy -Dmaven.test.skip=true"
                }
            }
            stage('Bump next dev version') {
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    bumpVersion("pom.xml", params.BUMP, "-SNAPSHOT")
                }
            }
            stage('Clean Up') {
                steps {
                    deleteDir()
                }
            }
        }
    }
}
