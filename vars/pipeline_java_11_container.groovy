#!/usr/bin/env groovy

/**
 * Complete pipeline to build/package/containerize/tag/release a Maven/Java project using Apache Maven
 * Pipeline assumes existence of pom.xml and Dockerfile in repo root directory
 * Assumes Nexus_Credentials set in Jenkins
 */
def call (String imageName, String nexusUrl, String awsRegion, String awsAccountId, String awsRole) {
    pipeline {
        agent {
            ecs {
                inheritFrom 'fargate-jenkins-agent-java'
            }
        }
        parameters {
            choice(choices: 'Default\nRelease', description: 'Choose action?', name: 'ACTION')
            choice(choices: 'NA\nPATCH\nMINOR\nMAJOR', description: 'Bump for next snapshot version', name: 'BUMP')
        }
        stages {
            stage('Checkout') {
                steps {
                    deleteDir()
                    checkout scm
                }
            }
            stage('Build & unit test') {
                steps {
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        sh 'mvn clean compile test'
                    }
                }
            }
            stage('Set release version & tag repo') {
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    // Update the Maven build file to release version and tag in Git repo
                    tagRelease("pom.xml")
                }
            }
            stage('Assemble') {
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature-*'
                    }
                }
                steps {
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        sh 'mvn package -DskipTests=true'
                    }
                    stash includes: 'pom.xml', name: 'BUILD_MAVEN'   // Stash the pom.xml in case version updated
                    stash includes: 'target/*.jar', name: 'APP_JAR'  // Stash the application jar
                }
            }
            stage('Build container') {
                agent { label 'docker-daemon' }  // Run the docker build on the Jenkins master
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature-*'
                    }
                }
                steps {
                    sh "rm -rf target"  // Remove the target folder to prevent leftover artefacts
                    unstash 'BUILD_MAVEN'
                    unstash 'APP_JAR'
                    // Build the container
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        dockerBuild(imageName, nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
                    }
                }
            }
            stage('Publish to Nexus') {
                agent { label 'docker-daemon' }  // Run the docker build on the Jenkins master
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature*'
                    }
                }
                steps {
                    unstash 'BUILD_MAVEN'
                    // Publish to Nexus
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        dockerPublishNexus(imageName, "pom.xml", nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
                    }
                }
            }
            stage('Release to ECR') {
                agent { label 'docker-daemon' }  // Publish from Jenkins master
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    unstash 'BUILD_MAVEN'
                    withAWS(roleAccount: awsAccountId, role: awsRole) {
                        // Publish to AWS ECR
                        dockerPublishAwsEcr(imageName, "pom.xml", awsRegion, awsAccountId)
                    }
                }
            }
            stage('Bump next dev version') {
                agent { label 'docker-daemon' }  // Bump version on Jenkins master
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    // Bump the build file to snapshot version and push to Git repo
                    bumpVersion("pom.xml", params.BUMP, "-SNAPSHOT")
                }
            }
            stage('Deploy') {
                agent { label 'docker-daemon' }  // Deploy from Jenkins master
                when {
                    allOf { branch 'master' }
                }
                steps {
                    build job: 'WLUKI-Bitbucket/cto-ddu-mmp-environment/test-environment', wait: false
                }
            }
            stage('Clean up') {
                agent { label 'docker-daemon' }  // Run cleanup on the Jenkins master
                when {
                    allOf { branch 'master' }
                }
                steps {
                    dockerPrune(imageName)
                    deleteDir()
                }
            }
        }
    }
}
