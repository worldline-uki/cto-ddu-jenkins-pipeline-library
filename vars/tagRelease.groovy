#!/usr/bin/env groovy
import com.worldline.BuildFile
import com.worldline.Git

/**
 * Tag the code repository with the release build version
 */
def call (String buildFilePath, boolean includeNameInTag = false) {

  def git = new Git()
  def buildFile = new BuildFile()

  def (String name, String baseVersion, String feature, String snapshot) = buildFile.getNameAndVersion(buildFilePath)

  // Update the build file to the "release" version (i.e. discarding feature/snapshot suffix)
  buildFile.updateVersion(buildFilePath, baseVersion)

  // Create the git tag - tag with app name if multi-repo
  def releaseTag
  if (includeNameInTag) {
    releaseTag = "${name}-${baseVersion}"
  } else {
    releaseTag = baseVersion
  }

  // Commit the updated build file (note -> no push)
  // This commit will be on the TAG branch as part of TAG push
  // Pushing here will cause a conflict when the updated release file is written back to repo as part of version bump
  // It also triggers an immediate 2nd build of the release version!
  def releaseCommitMessage = "Tagged release ${releaseTag} from ${baseVersion}${feature}${snapshot}"
  git.gitCommit(buildFilePath, releaseCommitMessage)

  // Tag the "release" version in the Git repo
  git.tagVersion(releaseTag)
}
