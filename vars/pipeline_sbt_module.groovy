#!/usr/bin/env groovy

/**
 * Complete pipeline to build/package/tag/release a sbt/scala module
 * Pipeline assumes existence of build.sbt in repo root directory
 * Assumes Nexus_Credentials set in Jenkins
 */
def call () {
  pipeline {
    agent {
      ecs {
        inheritFrom 'fargate-jenkins-agent-sbt'
//      memory 1024
//      cpu 256
      }
    }
    parameters {
      choice(choices: 'Default\nRelease', description: 'Choose action?', name: 'ACTION')
      choice(choices: 'NA\nPATCH\nMINOR\nMAJOR', description: 'Bump for next snapshot version', name: 'BUMP')
      choice(choices: 'DefinitelyNot\nForcePublish', description: 'Force publish ignoring unit test results', name: 'FORCE_PUBLISH')
    }
    stages {
      stage('Checkout') {
        steps {
          deleteDir()
          checkout scm
        }
      }
      stage('Build & Unit Test') {
        when {
          allOf {
            not { expression { return params.FORCE_PUBLISH.equalsIgnoreCase("ForcePublish") } }
          }
        }
        steps {
          withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
            sh 'sbt clean coverage test coverageReport'
          }
        }
      }
      stage('Set release version & tag repo') {
        when {
          allOf {
            branch 'master'
            expression { return params.ACTION.equalsIgnoreCase("Release") }
          }
        }
        steps {
          tagRelease("build.sbt")
        }
      }
      stage('Publish module') {
        when {
          anyOf {
            branch 'master'
            branch 'feature*'
            expression { return params.FORCE_PUBLISH.equalsIgnoreCase("ForcePublish") }
          }
        }
        steps {
          withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
            sh "sbt publish"
          }
        }
      }
      stage('Bump next dev version') {
        when {
          allOf {
            branch 'master'
            expression { return params.ACTION.equalsIgnoreCase("Release") }
          }
        }
        steps {
          bumpVersion("build.sbt", params.BUMP, "-SNAPSHOT")
        }
      }
      stage('Clean Up') {
        steps {
          deleteDir()
        }
      }
    }
  }
}

