#!/usr/bin/env groovy
import com.worldline.Docker

/**
 * Deploy versioned services to docker swarm for specified environment
 * Requires environment vars NEXUS_URL, NEXUS_USER and NEXUS_PASSWORD
 */

def call(String envPrefix) {

  println("Branch is ${env.BRANCH_NAME}")
  def environmentString = "${env.BRANCH_NAME}" as String
  def targetEnv = (environmentString =~ /(.*)-environment/)[0][1] as String
  println("Target environment is ${targetEnv}")
  def envConfig = readJSON file: 'environments.json'
  def envDef = envConfig[targetEnv]
  if (envDef == null) {
    println("ERROR - Unable to find definition for target environment ${targetEnv} in environments.json")
    throw new Exception("ERROR - Unable to find definition for target environment ${targetEnv} in environments.json")
  }
  def envType = envDef.envType
  println("Environment type is ${envType}")
  switch (envType) {
    case "SWARM":
      withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
        deploySwarmEnvironment(envConfig[targetEnv].stackId, envConfig[targetEnv].dockerHost, envConfig[targetEnv].nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
      }
      break
    case "FARGATE":
      String iamRole = envConfig[targetEnv].iamRole
      if (iamRole) {
        println("Deploying to Fargate with IAM Role ${iamRole}.")
        withAWS(roleAccount: envConfig[targetEnv].awsAccountId, role: iamRole) {
          deployFargateEnvironment(envPrefix, targetEnv)
        }
      } else {
        println("Deploying to Fargate without IAM Role.")
        withAWS(roleAccount: envConfig[targetEnv].awsAccountId) {
          deployFargateEnvironment(envPrefix, targetEnv)
        }
      }
      break
    default:
      println("Unknown environment type [${envType}] - unable to deploy")
      break
  }
}
