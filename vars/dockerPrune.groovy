#!/usr/bin/env groovy
import com.worldline.Docker

/**
 * Bump version number in build file
 * Note - Must have $NEXUS_PASSWORD and $NEXUS_USER env vars set
 */
def call(String imageName) {

  def docker = new Docker()
  docker.dockerPrune(imageName)
}
