#!/usr/bin/env groovy

/**
 * Complete pipeline to build/package/containerize/tag/release a sbt/scala project using universal packager
 * Pipeline assumes existence of build.sbt and Dockerfile in repo root directory
 * Assumes Nexus_Credentials set in Jenkins
 * - imageName - Name of the docker image
 * - nexusUrl - Url of the nexus to which docker image to be published.
 * - publishTargets - Map with details of all ECR targets.
 */
def call (String imageName, String nexusUrl, Map publishTargets) {
    pipeline {
        agent {
            ecs {
                inheritFrom 'fargate-jenkins-agent-sbt'
                //  memory 1024
                //  cpu 256
            }
        }
        parameters {
            choice(choices: 'Default\nRelease', description: 'Choose action?', name: 'ACTION')
            choice(choices: 'NA\nPATCH\nMINOR\nMAJOR', description: 'Bump for next snapshot version', name: 'BUMP')
        }
        stages {
            stage('Checkout') {
                steps {
                    checkout scm
                }
            }
            stage('Build & unit test') {
                steps {
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        sh 'sbt clean coverage test coverageReport'
                    }
                }
            }
            stage('Set release version & tag repo') {
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    // Update the SBT build file to release version and tag in Git repo
                    tagRelease("build.sbt")
                }
            }
            stage('Package Scala app') {
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature-*'
                    }
                }
                steps {
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        sh 'sbt universal:packageBin'
                    }
                    stash includes: 'build.sbt', name: 'BUILD_SBT'             // Stash the .sbt in case version updated
                    stash includes: 'target/universal/*.zip', name: 'APP_ZIP'  // Stash the universal packager zip output file
                }
            }
            stage('Build container') {
                agent { label 'docker-daemon' }  // Run the docker build on the Jenkins master
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature-*'
                    }
                }
                steps {
                    sh "rm -rf target"  // Remove the target folder to prevent leftover artefacts
                    unstash 'BUILD_SBT'
                    unstash 'APP_ZIP'
                    // Build the container
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        dockerBuild(imageName, nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
                    }
                }
            }
            stage('Publish to Nexus') {
                agent { label 'docker-daemon' }  // Run the docker build on the Jenkins master
                when {
                    anyOf {
                        branch 'master'
                        branch 'feature*'
                    }
                }
                steps {
                    unstash 'BUILD_SBT'
                    // Publish to Nexus
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        dockerPublishNexus(imageName, "build.sbt", nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
                    }
                }
            }
            stage('Release to ECR') {
                agent { label 'docker-daemon' }  // Publish from Jenkins master
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    unstash 'BUILD_SBT'
                    script {
                        publishTargets.each { entry ->
                            stage (entry.value.awsAccountDescription) {
                                withAWS(roleAccount: entry.key, role: entry.value.awsRole, externalId: entry.value.externalId) {
                                    // Publish to AWS ECR
                                    dockerPublishAwsEcr(imageName, "build.sbt", entry.value.awsRegion, entry.key, entry.value.namePrefix)
                                }
                            }
                        }
                    }
                }
            }
            stage('Bump next dev version') {
                agent { label 'docker-daemon' }  // Bump version on Jenkins master
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    // Bump the build file to snapshot version and push to Git repo
                    bumpVersion("build.sbt", params.BUMP, "-SNAPSHOT")
                }
            }
            stage('Deploy') {
                agent { label 'docker-daemon' }  // Deploy from Jenkins master
                when {
                    allOf { branch 'master' }
                }
                steps {
                    build job: 'WLUKI-Bitbucket/cto-ddu-mmp-environment/test-environment', wait: false
                }
            }
            stage('Clean up') {
                agent { label 'docker-daemon' }  // Run cleanup on the Jenkins master
                when {
                    allOf { branch 'master' }
                }
                steps {
                    dockerPrune(imageName)
                    deleteDir()
                }
            }
        }
    }
}
