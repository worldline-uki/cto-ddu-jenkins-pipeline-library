#!/usr/bin/env groovy
import com.worldline.Git
import com.worldline.BuildFile

/**
 * Bump SNAPSHOT version number in build file and commit to Git repo
 */
def call (String buildFilePath, String bump = "PATCH", String suffix = "-SNAPSHOT") {

  def git = new Git()
  def buildFile = new BuildFile()

  def (String name, String baseVersion, String feature, String snapshot) = buildFile.getNameAndVersion(buildFilePath)
  def currentVersion = "${baseVersion}${feature}${snapshot}"

  // Bump the version (add -SNAPSHOT development suffix)
  // Get the current version from the build file - ignore snapshot/feature
  def newVersion = buildFile.bump(buildFilePath, bump, suffix)
  println "New version (${bump}) is $newVersion"

  // Commit the updated build file
  def commitMessage = "Bumped development snapshot version from ${currentVersion} to new ${bump} version ${newVersion}"
  git.gitCommit(buildFilePath, commitMessage)
  git.gitPush() // Explicit push for change to trigger build of new dev version
}
