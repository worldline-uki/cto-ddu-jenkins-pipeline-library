#!/usr/bin/env groovy

/**
 * Complete pipeline to build/package/containerize/tag/release a Node project using Node Package Manager
 * Pipeline assumes existence of package.json and Dockerfile in repo root directory
 * Assumes Nexus_Credentials set in Jenkins
 */
def call (String imageName, String nexusUrl, String awsRegion, String awsAccountId, String awsRole, String namePrefix = "") {
    pipeline {
        agent {
            ecs {
                inheritFrom 'fargate-jenkins-agent-node'
            }
        }
        parameters {
            choice(choices: 'Default\nRelease', description: 'Choose action?', name: 'ACTION')
        }
        stages {
            stage('Checkout') {
                steps {
                    checkout scm
                }
            }
            stage('Install dependencies') {
                steps {
                    sh 'npm install --no-optional --loglevel verbose'
                }
            }
            stage('Package') {
                steps {
                    sh 'npm run build'
                    zip dir: 'dist', glob: '', zipFile: 'sim-dist.zip'
                    stash includes: 'sim-dist.zip,package.json', name: 'DIST_DIR'
                }
            }
            stage('Build container') {
                agent { label 'docker-daemon' }  // Run the docker build on the Jenkins master
                when {
                    anyOf {
                        branch 'master'
                    }
                }
                steps {
                    sh "rm -rf dist"  // Remove the target folder to prevent leftover artefacts
                    unstash 'DIST_DIR'
                    unzip zipFile: 'sim-dist.zip', dir: 'dist'
                    // Build the container
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        dockerBuild(imageName, nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
                    }
                }
            }
            stage('Publish to Nexus') {
                agent { label 'docker-daemon' }  // Run the docker build on the Jenkins master
                when {
                    anyOf {
                        branch 'master'
                    }
                }
                steps {
                    unstash 'DIST_DIR'
                    // Publish to Nexus
                    withCredentials([usernamePassword(credentialsId: 'Nexus_Credentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        dockerPublishNexus(imageName, "package.json", nexusUrl, env.NEXUS_USER, env.NEXUS_PASSWORD)
                    }
                }
            }
            stage('Release to ECR') {
                agent { label 'docker-daemon' }  // Publish from Jenkins master
                when {
                    allOf {
                        branch 'master'
                        expression { return params.ACTION.equalsIgnoreCase("Release") }
                    }
                }
                steps {
                    unstash 'DIST_DIR'
                    withAWS(roleAccount: awsAccountId, role: awsRole) {
                        // Publish to AWS ECR
                        dockerPublishAwsEcr(imageName, "package.json", awsRegion, awsAccountId, namePrefix)
                    }
                }
            }
            stage('Clean Up') {
                steps {
                    deleteDir()
                }
            }
        }
    }
}
