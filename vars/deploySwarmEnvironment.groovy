#!/usr/bin/env groovy
import com.worldline.Docker

/**
 * Deploy versioned services to docker swarm for specified environment
 * Requires environment vars NEXUS_URL, NEXUS_USER and NEXUS_PASSWORD
 */
def call(String dockerStackId, String dockerHost, String nexusUrl, String nexusUsername, String nexusPassword,
         String baseStackDef = "swarm/docker-stack-base.yml", String branchStackDef = "swarm/docker-stack.yml") {

  def docker = new Docker()

  println("Creating temporary docker context for ${dockerStackId} against host ${dockerHost}")
  String tempContext = org.apache.commons.lang.RandomStringUtils.random(9, true, true)
  docker.createDockerContext(tempContext, dockerHost)

  println("Updating stack definition with branch versions & stack overrides")
  def versionedStackDef = docker.mergeStackDefinitions(baseStackDef, branchStackDef)

  println("Deploying swarm stack ${dockerStackId} to docker host ${dockerHost} via temporary context ${tempContext}")
  docker.dockerLoginNexus(tempContext, nexusUrl, nexusUsername, nexusPassword)
  docker.deploySwarm(tempContext, dockerStackId, versionedStackDef)

  println("Deeleting temporary context ${tempContext}")
  docker.deleteDockerContext(tempContext)

}
