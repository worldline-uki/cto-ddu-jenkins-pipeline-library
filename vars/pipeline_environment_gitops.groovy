#!/usr/bin/env groovy

/**
 * Pipeline to refresh versions/deploy config for container based deployments
 * Runs against branches named *-env
 * Assumes existence of environments.json in base directory and service-defs folder
 * Assumes Nexus_Credentials set in Jenkins
 */
def call (String envPrefix) {
  pipeline {
    agent { label 'docker-daemon' }  // Run deployment on Jenkins master
    stages {
      stage('Checkout') {
        steps {
          checkout scm
        }
      }
      stage('Deploy Environment') {
        when {
          branch '*-environment'
        }
        steps {
          deployEnvironment(envPrefix)
        }
      }
      stage('Clean up') {
        steps {
          deleteDir()
        }
      }
    }
  }
}
