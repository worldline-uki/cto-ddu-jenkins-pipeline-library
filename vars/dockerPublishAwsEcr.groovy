#!/usr/bin/env groovy
import com.worldline.BuildFile
import com.worldline.Docker

/**
 * Publish an image to AWS ECR - image name/tag created from BuildFile metadata
 * builtImageName - Name of already built image to be published
 * buildFilePath - Path of build file to identify name and version
 * awsRegion, awsAccount - AWS region and account identifiers
 * namePrefix - If set, prefixed to published image name
 */
def call(String builtImageName, String buildFilePath, String awsRegion, String awsAccount, String namePrefix = "") {

    def docker = new Docker()
    def buildFile = new BuildFile()

    // Retrieve name/version information from build file
    def (String name, String baseVersion, String feature, String snapshot) = buildFile.getNameAndVersion(buildFilePath)

    // Set target image name for publishing incl full repo name
    String publishName = (namePrefix != "") ? "${namePrefix}-${name}" : name
    String awsEcr = "${awsAccount}.dkr.ecr.${awsRegion}.amazonaws.com"
    String tag = "${awsEcr}/${publishName}:${baseVersion}${feature}${snapshot}"

    println "Tagging docker image as ${tag}"
    docker.dockerLoginEcr(awsRegion, awsEcr)
    docker.dockerTagAndPush(builtImageName, tag)
}
