# jenkins-pipeline-library #
Utility scripts for jenkins pipelines/jobs/steps to be imported into Jenkins as a shared pipeline library.

/vars - Definition of custom steps for jenkins declarative pipelines
/vars - Definition of custom steps for jenkins declarative pipelines

/src  - ```Common.groovy``` - common functions for pipelines 

# Conventions for environment pipelines
The Jenkins environment pipelines expect the following files/folders to be present in the repository where the pipelines are invoked:

- **Branch naming**
Branch names must follow standard `*-environment` e.g `prod-environment`.  The suffix (e.g. `prod`) is used to identify the definition for the environment.
- **environments.json**
Defines target environments, names must match branch name convention.
- **versions**
Defines versions of services as a key value pair composed of `serviceName=serviceVersion`.
- **service-defs/***
Contains definition for each service referenced in `versions`.  File name `serviceName.json` expected for each reference.
